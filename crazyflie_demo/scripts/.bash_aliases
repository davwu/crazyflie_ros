
gsettings set org.gnome.desktop.lockdown disable-lock-screen 'true'

source ~/31390_ws/devel/setup.bash

alias myip="ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'"

#export ROS_IP=$(myip)
export ROS_IP=127.0.0.1
export ROS_MASTER_URI=http://$ROS_IP:11311

echo ROS_MASTER_URI: $ROS_MASTER_URI
echo ROS_IP: $ROS_IP

cfstart () {
  roslaunch crazyflie_demo uas_31390_crazyflie_server.launch channel:=$1
}

optitrack () {
  roslaunch crazyflie_demo optitrack.launch marker:=$1
}

alias rviz="rviz -d ~/.rviz/rviz.rviz"
alias battery="rostopic echo /crazyflie/battery"

