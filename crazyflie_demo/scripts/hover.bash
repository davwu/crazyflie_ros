#/bin/bash

rostopic pub /crazyflie/cmd_position geometry_msgs/Pose "position:
  x: 1.0
  y: 1.0
  z: 0.4
orientation:
  x: 0.0
  y: 0.0
  z: 0.0
  w: 0.4" -r 25

