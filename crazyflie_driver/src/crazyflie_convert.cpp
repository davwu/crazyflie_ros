
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PointStamped.h>

ros::Publisher pub;
geometry_msgs::PointStamped external_position;

void psCallback(const geometry_msgs::PoseStampedConstPtr& msg){
  external_position.header = msg->header;
  external_position.point = msg->pose.position;
  pub.publish(external_position);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "pose2position");
  ros::NodeHandle nh;

  pub = nh.advertise<geometry_msgs::PointStamped>("external_position", 1);

  ros::Subscriber sub = nh.subscribe("external_data", 1, &psCallback);

  ros::spin();
  return 0;
};


